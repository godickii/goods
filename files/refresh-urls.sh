#!/bin/bash
# Качаем файлы из списка url-ов
cd "${0%/*}"

if [ $# -lt 1 ]
then
  echo "argument required"
  exit 1
fi



input="$1"
output="$2"
while IFS= read -r url
do
  output_file_name=`echo "$url"|awk -F/ {'print $3}'`."xml"
  wget --no-check-certificate $url -O- | xmllint --format - > $output_file_name
done < "$input"
