# Чё это?

Парсинг и поиск по товарам из YML (yandex market language). Прототип.

# А есть где посмотреть?

Можно попробовать [сходить по этой ссылке](http://goods-prototype.lesha.info/), вдруг работает, но это не точно.

# Отказ от функциональности

Следующая функциональность будет требоваться в готовом продукте но на этапе прототипа не поддерживается:

* учёт наличия/возможности доставки по городам
* объединение одинаковых товарных позиций в одну карточку, как у ЯМ
* поиск по характеристикам

# Установка

Ставим python 3.8.2, например через [pyenv](https://github.com/pyenv/pyenv-installer)

```
$ pyenv install 3.8.2
$ pyenv local 3.8.2
```

Настраиваем virtualenv:
```
python -m venv venv
source venv/bin/activate
pip install --upgrade pip wheel
pip install -r requirements-dev.txt
```

Библиотечки для postgresql:
```
sudo apt install libpq-dev
```

# Создаём БД

Ставим postgresql и настраиваем БД:

```
$ psql -U postgres
create database goods;
create user goods;
alter user goods with password '1';
alter user goods with superuser;
```

# Создаём начальные данные

Мигрируем Дазу Банных

`$ src/manage.py migrate`

Создаём админа

`$ src/manage.py createsuperuser`

Заполняем БД предустановленными магазинами и правилами отобраежения категорий:

`$ src/manage.py create_initial_shops`

Загружаем фиды:

`$ src/manage.py update_goods_from_feeds`

Чтобы не загружать фиды снова, а брать из кэша (директория feed_cache), во второй раз можно запускать так:

`$ src/manage.py update_goods_from_feeds --skip-download`

Если нужно повторно обработать только один магазин, можно сделать так:

`$ src/manage.py update_goods_from_feeds --only-shop=1234`


# Запуск (машина разработчика)

`$ src/manage.py runserver`

и идём на главную страницу http://localhost:8000/ читать дальше

