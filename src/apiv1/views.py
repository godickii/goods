import functools

from django.contrib.postgres.search import SearchQuery, SearchRank
from rest_framework import generics
from django_filters import rest_framework as filters
from rest_framework.filters import OrderingFilter

from . import serializers
from goods.models import Category, Offer


RussianSearchQuery = functools.partial(SearchQuery, config="pg_catalog.russian")



class CategoriesListView(generics.ListAPIView):
    """
    Возвращает список категорий товаров. Фильтров (пока) нет, пагинации (пока) нет, вложенных категорий (пока) нет.
    """
    queryset = Category.objects.all()
    serializer_class = serializers.CategorySerializer
    pagination_class = None


class OffersFilter(filters.FilterSet):
    min_price = filters.NumberFilter(field_name="price", lookup_expr='gte')
    max_price = filters.NumberFilter(field_name="price", lookup_expr='lte')
    min_price_per_month = filters.NumberFilter(field_name="price_per_month", lookup_expr='gte')
    max_price_per_month = filters.NumberFilter(field_name="price_per_month", lookup_expr='lte')
    min_months = filters.NumberFilter(field_name="months", lookup_expr='gte')
    max_months = filters.NumberFilter(field_name="months", lookup_expr='lte')
    search = filters.CharFilter(method='filter_search')
    recommended = filters.BooleanFilter(method='sorting_by_promotional', field_name='is_promotional')

    def sorting_by_promotional(self, initial_queryset, name, value):
        return initial_queryset.order_by('-is_promotional') if value else initial_queryset

    def filter_search(self, initial_queryset, name, value):
        queryset = self._filter_search_queryset_with_value(initial_queryset, value)

        if queryset.count() == 0:
            queryset = self._get_minimal_non_empty_queryset(initial_queryset, value)

        queryset = queryset.order_by(
            '-search_rank',
            'categories_path',
        )

        return queryset

    @staticmethod
    def _filter_search_queryset_with_value(queryset, value):
        query = RussianSearchQuery(value)

        queryset = queryset.annotate(
            search_rank=SearchRank('search', query)
        ).filter(
            search=query
        )
        return queryset

    def _get_minimal_non_empty_queryset(self, initial_queryset, value):
        words = value.split()
        filtered_words = []

        for word in words:
            queryset = self._filter_search_queryset_with_value(initial_queryset, word)
            if queryset.count() > 0:
                filtered_words.append(word)

        value = ' '.join(filtered_words)
        queryset = self._filter_search_queryset_with_value(initial_queryset, value)
        return queryset

    class Meta:
        model = Offer
        fields = ['category']


class OffersListView(generics.ListAPIView):
    """
    # Возвращает список предложений.

    Пагинация по 100 штук.

    ### Поля:

    * name - имя товара
    * vendor - производитель товара
    * model - модель товара
    * price - общая цена товара в рублях
    * months - количество месяцев для рассрочкиv
    * price_per_month - платёж в месяц
    * picture - ссылка на картинку
    * partner_url - ссылка на карточку товара в магазине партнёра

    ## GET-параметры:

    ### сортировка:

    * ordering=price | -price | price_per_month | -price_per_month | months | -months

    ### фильтрация:

    * category
    * min_price
    * max_price
    * min_price_per_month
    * max_price_per_month
    * min_months
    * max_months

    Товары добыты нами из разных источников.
    Кое-где указан name, кое-где указан vendor и model.

    Мы предполагаем, что если указан name, лучше показывать пользователю его,
    иначе - vendor<пробел>model. Во втором случае это можно визуально представить
    иначе, а также можно предусмотреть поиск отдельно по vendor
    """
    queryset = Offer.objects.filter(is_visible=True, category__isnull=False)
    serializer_class = serializers.OfferSerializer
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)
    filterset_class = OffersFilter
    #ordering_fields = 'name', 'vendor', 'model', 'price', 'price_per_month', 'picture'
    #ordering = ['username11']
    # TODO: default order - shop.priority

