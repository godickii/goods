
from rest_framework import serializers
from rest_framework.reverse import reverse
from drf_queryfields import QueryFieldsMixin

from goods.models import Category, Offer


class CategorySerializer(serializers.ModelSerializer):
    browse_category_url = serializers.SerializerMethodField(read_only=True)

    def get_browse_category_url(self, obj):
        request = self.context['request']
        return reverse('apiv1:offers-list',
                       args=[],
                       request=request) + f'?category={obj.id}'

    class Meta:
        model = Category
        fields = (
            'id',
            'parent_id',
            'name',
            'browse_category_url',
        )


class OfferSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    search_rank = serializers.SerializerMethodField()
    category = CategorySerializer()
    category_name = serializers.SerializerMethodField()

    @staticmethod
    def get_category_name(obj):
        return obj.category.name

    @staticmethod
    def get_search_rank(obj):
        return obj.search_rank if hasattr(obj, 'search_rank') else None

    class Meta:
        model = Offer
        fields = (
            'id',
            'partner_offer_id',
            'categories_path',
            'category_name',
            'category',
            'vendor',
            'model',
            'name',
            'price',
            'months',
            'price_per_month',
            'picture',
            'partner_url',
            'search_rank',
        )
