from django.urls import path

from . import views

app_name = 'apiv1'

urlpatterns = [
    path(r'categories/', views.CategoriesListView.as_view(), name='categories-list'),
    path(r'offers/', views.OffersListView.as_view(), name='offers-list'),
]
