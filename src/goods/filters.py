from django.contrib.admin import SimpleListFilter

from goods.models import Category


class CategoryFilter(SimpleListFilter):
    title = 'Категория'
    parameter_name = 'category'

    def lookups(self, request, model_admin):
        categories = Category.objects.all()
        return [('all', 'Любая')] + [('null', 'Отсутствует')] + [(c.id, str(c)) for c in categories]

    def queryset(self, request, queryset):
        if self.value() == 'all':
            return queryset.filter(category__isnull=False)
        if self.value() == 'null':
            return queryset.filter(category__isnull=True)
        if self.value():
            return queryset.filter(category__id__exact=self.value())