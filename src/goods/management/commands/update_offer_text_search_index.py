
from django.core.management.base import BaseCommand

from goods.models import Offer


class Command(BaseCommand):
    help = 'Начальное создание БД'

    def handle(self, *args, **options):
        Offer.objects.update_text_search_index()
