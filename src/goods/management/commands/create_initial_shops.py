import copy

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from rest_framework.utils import json

from goods.models import Shop, Offer, Category, CategoryMap, DEFAULT_INSTALLMENT_PLAN
from goods.fakedb import Categorizer
from goods import yml

from goods.installment import DEFAULT_INSTALLMENT_PLAN

# Начальная загрузка фидов, как только мы создали пустую базу
# происходит в два этапа:
# 1. Загрузка фидов из categorizing.py вместе с маппингом категорий
#    (использовался в первой версии прототипа)
# 2. Загрузка фидов ниже без категорий

# см задачу https://youtrack.sovcombank.ru/issue/hlvgds-16

FAKE_INSTALLMENT_PLAN = {1000: 6, 10000: 12}  # план для примера, от 100 рублей - 6 месяцев рассрочки, от 10000 рублей - 12

PARTNERS = (
    (
        'https://santehnika-online.ru/xml_feed/yandex/yml_export.xml',
        'Сантехника Online',
        FAKE_INSTALLMENT_PLAN,
        {
            'климат:кондиционеры': 'бытовая техника:кондиционеры',
            'мебель для ванной': 'мебель:ванная',
        }
    ),
    (
        'https://tvoydom.ru/upload/catalog/yamarket.yml',
        'Твой Дом',
        FAKE_INSTALLMENT_PLAN
    ),
    (
        'https://street-beat.ru/local/feed_data/st/gwardrobe_rf.xml',
        'Street Beat',
        FAKE_INSTALLMENT_PLAN
    ),
    (
        'http://feed.mgcom.ru/yandexmarket/hoff/moscow/dda93',
        'Hoff',
        FAKE_INSTALLMENT_PLAN,
        {
            'гостиная': 'мебель:гостиная',
            'спальня': 'мебель:спальня',
            'кухня': 'мебель:кухня',
        }
    ),
    (
        'http://feed.mgcom.ru/yandexmarket/svyaznoy/promofocus/04ee9',
        'Связной',
        FAKE_INSTALLMENT_PLAN,
        {
            'техника для дома:утюги': 'бытовая техника:утюги',
            'техника для дома:пылесосы': 'бытовая техника:пылесосы',
            'техника для дома:водонагреватели': 'бытовая техника:водонагреватели',
            'фотоаппараты': 'электроника:фотоаппараты',
            'мобильные телефоны': 'электроника:мобильные телефоны'
        }
    ),
    # ппц как долго грузится, пока отключим нахер
    # ('http://export.lamoda.ru/matryoshka/farm/feed/245_3fd9ea59.xml.gz', 'Lamoda', FAKE_INSTALLMENT_PLAN),
    (
        'https://ralf.ru/bitrix/catalog_export/Complead.xml',
        'Ralf Ringer',
        FAKE_INSTALLMENT_PLAN
    ),
    (
        'https://id-mebel.ru/netcat/modules/netshop/export/yandex/yandex_market_feed.xml',
        'Интер Дизайн',
        FAKE_INSTALLMENT_PLAN
    ),
    (
        'https://paoloconte.ru/bitrix/catalog_export/yandex_yml.php',
        'Паоло Конте',
        FAKE_INSTALLMENT_PLAN
    ),
    (
        'https://x-store.net/bitrix/catalog_export/xstore-halva.xml',
        'X Store',
        FAKE_INSTALLMENT_PLAN,
        {
            'наушники и колонки:беспроводные bluetooth наушники': 'электроника:наушники',
            'наушники и колонки:колонки:bluetooth колонки': 'электроника:наушники',
            'наушники и колонки:накладные наушники': 'электроника:наушники',
            'наушники и колонки:наушники вкладыши': 'электроника:наушники',
            'наушники и колонки:гарнитуры': 'электроника:наушники',
            'наушники и колонки:type-c наушники': 'электроника:наушники',
        }
    ),
    (
        'https://www.santehnika-room.ru/yml/t_market2.xml',
        'Сантехника Room',
        FAKE_INSTALLMENT_PLAN
    ),
    # отключил, нашёл лучше: ('https://xml.holodilnik.ru/xml/?from=halva', 'Holodilnik', FAKE_INSTALLMENT_PLAN),
    (
        'https://xml.holodilnik.ru/xml/?from=ozon',
        'Holodilnik',
        FAKE_INSTALLMENT_PLAN,
        {
            'бытовая техника для дома:техника для ухода за одеждой:утюги': 'бытовая техника:утюги',
            'бытовая техника для дома:техника для уборки:пылесосы': 'бытовая техника:пылесосы',
            'бытовая техника для дома:техника для уборки:пылесосы беспроводные': 'бытовая техника:пылесосы',
            'бытовая техника для дома:техника для уборки:пылесосы моющие': 'бытовая техника:пылесосы',
            'бытовая техника для дома:техника для уборки:роботы-пылесосы': 'бытовая техника:пылесосы',
            'бытовая техника для дома:климатическая техника:водонагреватели': 'бытовая техника:водонагреватели',
            'цифровая техника, товары для офиса:фото и видеосъемка:цифровые фотоаппараты': 'электроника:фотоаппараты',
            'цифровая техника, товары для офиса:смартфоны, планшеты и гаджеты:мобильные телефоны': 'электроника:телефоны',
            'холодильники, морозильники:однокамерные холодильники': 'бытовая техника:холодильники',
            'холодильники, морозильники:двухкамерные холодильники': 'бытовая техника:холодильники',
            'холодильники, морозильники:холодильники side by side': 'бытовая техника:холодильники',
            'холодильники, морозильники:многокамерные холодильники': 'бытовая техника:холодильники',
            'холодильники, морозильники:минихолодильники': 'бытовая техника:холодильники',
            'посудомоечные, стиральные машины:стиральные машины': 'бытовая техника:стиральные машины',
            'посудомоечные, стиральные машины:стиральные машины с сушкой': 'бытовая техника:стиральные машины',
            'телевизоры, dvd, аудио-видео:телевизоры': 'электроника:телевизоры',
            'бытовая техника для дома:климатическая техника:кондиционеры': 'бытовая техника:кондиционеры',
        }
    ),
    # SSL certificate verify failed: unable to get local issuer certificate:
    # ('https://www.timecode.ru/upload/export-data/halva-feed.xml', 'Тайм Код', FAKE_INSTALLMENT_PLAN),
    (
        'https://www.technopark.ru/price/halva-pop.xml',
        'Технопарк',
        FAKE_INSTALLMENT_PLAN,
        {
            'крупная бытовая:холодильники': 'бытовая техника:холодильники',
        }
    ),
    # 404:
    # ('https://www.585zolotoy.ru/cron/yandex_market_halva.xml', '585 Золотой', FAKE_INSTALLMENT_PLAN),
    (
        'https://www.akusherstvo.ru/xmlprice/halva.php',
        'Акушерство',
        FAKE_INSTALLMENT_PLAN
    ),
    (
        'http://feed.mgcom.ru/yandexmarket/knowhow/halva/f19c2',
        'Ноу Хау',
        FAKE_INSTALLMENT_PLAN,
        {
            'каталог товаров:смартфоны': 'электроника:мобильные телефоны',
        }
    ),
    (
        'https://www.rendez-vous.ru/public/market/halva.xml',
        'Рандеву',
        FAKE_INSTALLMENT_PLAN
    ),
    (
        'https://eyekraft.ru/download/xml_eyekraft_products',
        'Eye Крафт',
        FAKE_INSTALLMENT_PLAN
    ),
    (
        'https://zarina.ru/upload/xmlexport/halva.xml',
        'Зарина',
        FAKE_INSTALLMENT_PLAN
    ),
)


class Command(BaseCommand):
    help = 'Начальное создание БД'
    #TODO: сделать --confirm

    # def add_arguments(self, parser):
    #     parser.add_argument('poll_ids', nargs='+', type=int)

    def handle(self, *args, **options):
        category_map = copy.deepcopy(Categorizer.CATEGORY_MAP)

        # for filename, map_rules in category_map.items():
        #     meta = map_rules.pop('_meta')
        #     feed_url = meta['feed_url']
        #     with transaction.atomic():
        #
        #         shop, _ = Shop.objects.get_or_create(
        #             feed_url=feed_url,
        #             defaults=dict(
        #                 name=meta['name'],
        #                 installment_plan='{1000: 2, 5000: 4}',
        #                 priority=meta['priority'],
        #                 picture=meta['label_img']
        #             ),
        #         )
                # создаём отображение категорий
                # for p_c_c, dst in map_rules.items():
                #     CategoryMap.objects.get_or_create(
                #         category=Category.objects.make_or_get(path=dst),
                #         partner=shop,
                #         categories_path=p_c_c,
                #     )

                # for partner_category_path, our_category_name in map_rules.items():
                #     CategoryMap.objects.get_or_create(
                #         category=Category.objects.get_or_create(name=our_category_name)[0],
                #         partner=shop,
                #         categories_path=partner_category_path,
                #     )
                #
                # offer_callback = self._get_offer_callback_for_shop(shop)
                # yml.FileParser(offer_callback).parse_file(filename)

        # CategoryMap.objects.reindex()

        for tupl in PARTNERS:
            if len(tupl) == 3:
                feed_url, shop_name, installment_plan = tupl
                map_rules = {}
            else:
                feed_url, shop_name, installment_plan, map_rules = tupl

            shop, _ = Shop.objects.get_or_create(
                feed_url=feed_url,
                defaults=dict(
                    name=shop_name,
                    installment_plan=json.dumps(installment_plan),
                    priority=10,
                ),
            )
            shop.installment_plan = installment_plan
            shop.save()

            # создаём отображение категорий
            for p_c_c, dst in map_rules.items():
                CategoryMap.objects.get_or_create(
                    category=Category.objects.make_or_get(path=dst),
                    partner=shop,
                    categories_path=p_c_c,
                )

            CategoryMap.objects.reindex()


