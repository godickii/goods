import copy
import os.path
import traceback

import requests

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.conf import settings

from goods.models import Shop, Offer, Category, CategoryMap, DEFAULT_INSTALLMENT_PLAN
from goods.fakedb import Categorizer
from goods import yml


class Command(BaseCommand):
    help = 'Обновлить список товаров'

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            '--skip-download',
            action='store_true',
            help='Force use cache, don\'t download',
        )

        parser.add_argument(
            '--skip-parsing',
            action='store_true',
            help='Download only',
        )

        parser.add_argument(
            '--only-shop',
            action='store',
            help='Process only specific shop (by id)',
        )

    def download_file(self, url, local_filename):
        # NOTE the stream=True parameter below
        i = 0
        chunk_size = 100 * 1024
        with requests.get(url, verify=False, stream=True) as r:
            r.raise_for_status()
            with open(local_filename, 'wb') as f:
                for chunk in r.iter_content(chunk_size=chunk_size):
                    i += 1
                    print(f'.', end='', flush=1)
                    # self.stdout.write('... %d Kb' % (chunk_size * i / 1024))
                    # If you have chunk encoded response uncomment if
                    # and set chunk_size parameter to None.
                    # if chunk:
                    f.write(chunk)
        self.stdout.write('download done')
        return local_filename

    def handle(self, *args, **options):
        if not os.path.exists(settings.FEED_CACHE_DIR):
            os.mkdir(settings.FEED_CACHE_DIR)
        queryset = Shop.objects.all()

        if options.get('only_shop'):
            queryset = queryset.filter(id=options['only_shop'])

        for shop in queryset:
            self.current_shop = shop
            self.current_shop_created = 0
            self.current_shop_updated = 0
            self.current_shop_unchanged = 0
            url = shop.feed_url
            filename = shop.get_cache_file_path()
            self.stdout.write(f'*** Shop id {shop.id} {shop}')
            if not options['skip_download']:
                self.stdout.write(f'Downloading {url} to {filename}')
                try:
                    self.download_file(url, filename)
                except:
                    self.stdout.write(f'{shop}: download failed:')
                    self.stderr.write(traceback.format_exc())

            # for partner_category_path, our_category_name in map_rules.items():
            #     CategoryMap.objects.get_or_create(
            #         category=Category.objects.get_or_create(name=our_category_name)[0],
            #         partner=shop,
            #         categories_path=partner_category_path,
            #     )

            if not options['skip_parsing']:
                # запоминаем, какие товары есть сейчас, чтобы потом удалить те, которых
                # больше нет (которые не были затронуты при обновлении фида)
                self.current_shop_existing_offers = set(Offer.objects.filter(
                    partner=shop,
                ).values_list('partner_offer_id', flat=True))

                # текущий набор offer-ов
                self.current_shop_current_batch_offers = []

                #offer_callback = self._get_offer_callback_for_shop(shop)
                yml_parser = yml.FileParser(self.offer_callback)
                try:
                    yml_parser.parse_file(filename)
                    self.sync_current_batch_offers()
                except:
                    self.stdout.write(f'{shop}: parse failed:')
                    self.stderr.write(traceback.format_exc())
                else:
                    shop.category_tree = '\n'.join(yml_parser.get_printable_tree())
                    shop.save()

                    if self.current_shop_existing_offers:
                        print('Deleting %d offers which absent in feed...' % len(self.current_shop_existing_offers), end='', flush=1)
                        Offer.objects.filter(
                            partner=shop,
                            partner_offer_id__in=self.current_shop_existing_offers,
                        ).delete()
                        print('done')
                    print(f'\n{yml_parser._total_offers_count} feeds processed, {self.current_shop_created} offers created, {self.current_shop_unchanged} unchanged, {self.current_shop_updated} updated, {len(self.current_shop_existing_offers)} deleted.')

        print('reindex...', end='', flush=1)
        CategoryMap.objects.reindex()
        print('done')

    def offer_callback(self, offer):
        self.current_shop_current_batch_offers.append(offer)
        if len(self.current_shop_current_batch_offers) > 100:
            self.sync_current_batch_offers()

    def sync_current_batch_offers(self):
        """Отработать текущий набор товаров из фида - что-то создать, что-то обновить"""
        # print(self.current_shop_existing_offers)
        # print([o['id'] for o in self.current_shop_current_batch_offers])
        new_offer_ids = set(o['id'] for o in self.current_shop_current_batch_offers if o['id'] not in self.current_shop_existing_offers)

        offers_for_bulk_create = []
        offers_for_bulk_update_data = {}

        for offer in self.current_shop_current_batch_offers:
            # количество месяцев, для которых доступна рассрочка
            price = offer['price']
            months = None

            for cur_amount, cur_month in sorted(DEFAULT_INSTALLMENT_PLAN.items(), key=lambda pair: pair[0]):
                if price >= cur_amount:
                    months = cur_month

            if months:
                offer['months'] = months
                import math
                offer['price_per_month'] = int(math.ceil(price / months))

            if offer['id'] in self.current_shop_existing_offers:
                self.current_shop_existing_offers.remove(offer['id'])

            moderated_offer_data = {
                'name': offer.get('name', '') or '',
                'vendor': offer.get('vendor', '') or '',
                'model': offer.get('model', '') or '',
            }

            not_moderated_offer_data = {
                'price': price,
                'months': months,
                'price_per_month': offer.get('price_per_month', None),
                'picture': offer.get('picture', ''),
                'partner_url': offer.get('url', ''),
            }

            all_offer_data = {**moderated_offer_data, **not_moderated_offer_data}
            if offer['id'] in new_offer_ids:
                self.current_shop_created += 1
                offers_for_bulk_create.append(
                    Offer(
                        partner=self.current_shop,
                        partner_offer_id=offer['id'],
                        **all_offer_data,
                        categories_path=':'.join(offer['category_path'])
                    )
                )
            else:
                offers_for_bulk_update_data[offer['id']] = (moderated_offer_data, not_moderated_offer_data, all_offer_data)

        Offer.objects.bulk_create(
            offers_for_bulk_create
        )

        offers_for_bulk_update_stage1 = Offer.objects.filter(
            partner=self.current_shop,
            partner_offer_id__in=offers_for_bulk_update_data.keys(),
        )

        offers_for_bulk_update_stage2 = []

        for o in offers_for_bulk_update_stage1:
            (moderated_offer_data, not_moderated_offer_data, all_offer_data) = offers_for_bulk_update_data[o.partner_offer_id]
            bulk_update_fields = all_offer_data.keys()
            is_changed = False
            for k, v in moderated_offer_data.items():
                if getattr(o, k) != v:
                    o.is_visible = False
                    setattr(o, k, v)
                    is_changed = True
            for k, v in not_moderated_offer_data.items():
                if getattr(o, k) != v:
                    setattr(o, k, v)
                    is_changed = True

            if is_changed:
                #TODO: или если изменилась категория
                self.current_shop_updated += 1
                offers_for_bulk_update_stage2.append(o)
            else:
                self.current_shop_unchanged += 1

        if offers_for_bulk_update_stage2:
            Offer.objects.bulk_update(offers_for_bulk_update_stage2, bulk_update_fields)

        self.current_shop_current_batch_offers = []
