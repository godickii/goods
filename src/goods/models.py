import logging
import os.path

from django.contrib.postgres.indexes import GinIndex
from django.contrib.postgres.search import SearchVectorField, SearchVector
from django.db import models, transaction
from django.db.models.expressions import Value
from django.conf import settings

# Create your models here.

# TODO: план рассрочки фиксированный - избавиться
from goods.util import listify

DEFAULT_INSTALLMENT_PLAN = {100: 2, 1000: 3, 5000: 4, 10000: 6, 20000: 9, 30000: 12}
# от 100 рублей - 2 месяца рассрочки,
# от 1000 рублей - 3 месяца
# от 5 тысяч - 4,
# от 10 тысяч - 6
# и т.д.


# есть партнёры: до 10 тысяч - 2, после - 4


log = logging.getLogger()


class Shop(models.Model):
    """Партнёр."""
    name = models.CharField('Название', max_length=255)
    picture = models.CharField('Имя файла с картинкой', max_length=255)
    feed_url = models.CharField(
        'Url feed-а для загрузки товаров',
        max_length=1024,
        blank=True
    )
    created_at = models.DateTimeField(auto_now_add=True)
    is_promotional = models.BooleanField('Наличия акции', default=False)
    priority = models.PositiveIntegerField(
        'Приоритет',
        help_text='меньшее значение означает более высокие позиции при сортировке по умолчанию',
        default=10,
    )
    # пока не завязываемся на JsonField в postgresql
    installment_plan = models.CharField(
        'план рассрочки',
        help_text='формат "{1000: 2, 5000: 4}" - от 1000 рублей - 2 месяца рассрочки, от 5000 - 4 месяца',
        max_length=1024
    )
    category_tree = models.TextField(
        'дерево категорий в фиде партнёра',
        blank=True
    )

    objects = models.Manager()

    def get_cache_file_path(self):
        filename = self.feed_url.replace('https://', '').replace('http://', '').replace('/', '-').replace('?', '').replace('=', '-')
        return os.path.join(settings.FEED_CACHE_DIR, filename)

    def get_annotated_category_tree(self):
        def generate():
            for line in self.category_tree.split('\n'):
                category_path, amount = line.rsplit(' ', 1)
                cm = self.categorymap_set.filter(categories_path=category_path).first()
                if cm:
                    yield [category_path, amount, str(cm.category)]
                else:
                    yield [category_path, amount]

        return list(generate())

    class Meta:
        verbose_name = 'Магазин'
        verbose_name_plural = 'Магазины'

    def __str__(self):
        return f'{self.name}'

    def __repr__(self):
        return f'<{self.__class__.__name__} {self.name}>'


class CategoryManager(models.Manager):

    def make_or_get(self, path):
        parent = None
        for element in path.split(':'):
            category, _ = Category.objects.get_or_create(
                name=element,
                **(parent and {'parent': parent} or {'parent__isnull': True})
            )
            parent = category
        return category


class Category(models.Model):
    """Категория товара."""
    parent = models.ForeignKey("self", null=True, on_delete=models.CASCADE)
    name = models.CharField(
        'Название катоегории',
        help_text='Например, "бытовая техника:холодильники:двухкамерные',
        max_length=255,
        db_index=True
    )
    slug = models.SlugField()
    picture = models.CharField('Имя файла с картинкой', max_length=255)
    is_promotional = models.BooleanField('Наличия акции', default=False)

    objects = CategoryManager()

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        c = self
        result = f'{c.name}'
        for i in range(10):
            if c.parent:
                c = c.parent
                result = f'{c.name}:{result}'

        return result

    def __repr__(self):
        return f'<{self.__class__.__name__} {str(self)}>'


class OfferManager(models.Manager):
    def update_text_search_index(self):
        count = Offer.objects.count()
        log.info(f"got {count} offers in total")
        current = 0
        for offer in self.all().iterator():
            current += 1
            if not current % 1000:
                log.info(f"{current} offers processed")
            offer.update_text_search_index()

    def create(self, **kwargs):
        """
        Create a new object with the given kwargs, saving it to the database
        and returning the created object.
        """
        update_text_search_index = kwargs.pop('update_text_search_index', True)
        obj = self.model(**kwargs)
        self._for_write = True
        obj.save(force_insert=True,
                 using=self.db,
                 update_text_search_index=update_text_search_index)
        return obj

    def get_or_create(self, defaults=None, **kwargs):
        """
        Look up an object with the given kwargs, creating one if necessary.
        Return a tuple of (object, created), where created is a boolean
        specifying whether an object was created.
        """
        # The get() needs to be targeted at the write database in order
        # to avoid potential transaction consistency problems.
        return super().get_or_create(defaults, **kwargs)

    def update_or_create(self, defaults=None, **kwargs):
        """
        Look up an object with the given kwargs, updating one with defaults
        if it exists, otherwise create a new one.
        Return a tuple (object, created), where created is a boolean
        specifying whether an object was created.
        """
        return super().update_or_create(defaults, **kwargs)


class Offer(models.Model):
    """Товар."""
    partner = models.ForeignKey(Shop, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True, blank=True)
    partner_offer_id = models.CharField('Идентификатор товара у партнёра', max_length=30)
    name = models.CharField(max_length=255, db_index=True, blank=True)
    vendor = models.CharField(max_length=255, db_index=True, blank=True)
    model = models.CharField(max_length=255, db_index=True, blank=True)
    categories_path = models.CharField(
        'Путь категории, разделённый двоеточием, в исходном файле',
        help_text='Например, "бытовая техника:холодильники:двухкамерные',
        max_length=255,
        db_index=True,
    )
    price = models.PositiveIntegerField(
        'Цена',
        db_index=True
    )
    months = models.PositiveIntegerField(
        'Количество месяцев рассрочки',
        db_index=True,
        null=True
    )
    price_per_month = models.PositiveIntegerField(
        'Платёж в месяц',
        db_index=True,
        null=True
    )
    picture = models.CharField('Имя файла с картинкой', max_length=1023, blank=True)
    partner_url = models.CharField('Ссылка на карточку товара в магазине партнёра', max_length=1023)

    # модерация
    is_visible = models.BooleanField('Проверено', default=False)
    previous_data = models.TextField('Предыдущие данные', max_length=255, db_index=True, blank=True)
    previous_data_hash = models.CharField('Хэш предыдущих данных', max_length=64, db_index=True, blank=True)
    is_promotional = models.BooleanField('Наличия акции', default=False)

    search = SearchVectorField(
        verbose_name="Поле для полнотекстового поиска товара по некоторым атрибутам",
        null=True,
        blank=True
    )

    objects = OfferManager()

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'
        indexes = [
            # Note that GIN index build time can often be improved by increasing maintenance_work_mem,
            # while GiST index build time is not sensitive to that parameter.
            GinIndex(fields=['search'])
        ]

    def save(self, *args, **kwargs):
        text_search_vector_fields = {
            'name',
            'vendor',
            'model',
            'categories_path',
        }
        update_fields = kwargs.pop('update_fields', None)
        update_text_search_index = kwargs.pop('update_text_search_index', True)

        if (
                (
                        update_fields is None or
                        text_search_vector_fields.intersection(update_fields)
                ) and
                update_text_search_index
        ):
            self.update_text_search_index(save=False)

            if update_fields is not None:
                kwargs["update_fields"] = tuple(update_fields) + ("search",)

        return super().save(*args, **kwargs)

    def update_text_search_index(self, save=True):
        self.search = SearchVector(
            Value(self.name, output_field=models.TextField()),
            Value(self.vendor, output_field=models.TextField()),
            Value(self.model, output_field=models.TextField()),
            Value(self.categories_path, output_field=models.TextField()),
            config="pg_catalog.russian"
        )
        if save:
            super().save(update_fields=('search',))


class CategoryMapManager(models.Manager):

    #@transaction.atomic
    def reindex(self):
        # TODO: предусмотреть частичный reindex и вызывать его в соответствии с действиями в админке
        # TODO: ах да, нужна админка для отображения категорий

        # TODO: очень большая транзакция, одновременно с загрузкой бывает это:
        # django.db.utils.OperationalError: ОШИБКА:  обнаружена взаимоблокировка
        # DETAIL:  Процесс 22856 ожидает в режиме ShareLock блокировку "транзакция 573429"; заблокирован процессом 22254.
        # Процесс 22254 ожидает в режиме ShareLock блокировку "транзакция 573437"; заблокирован процессом 22856.
        # HINT:  Подробности запроса смотрите в протоколе сервера.
        # CONTEXT:  при удалении кортежа (0,1) в отношении "goods_offer"

        Offer.objects.all().update(category=None)
        for category_map in self.all().iterator():
            Offer.objects.filter(
                partner=category_map.partner,
                categories_path=category_map.categories_path
            ).update(
                category_id=category_map.category.id,
            )


class CategoryMap(models.Model):
    partner = models.ForeignKey(Shop, on_delete=models.CASCADE)
    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE,
        verbose_name='Категория в нашем каталоге'
    )
    categories_path = models.CharField(
        'Путь в feed-е партнёра, разделённый двоеточием',
        help_text='Например, "бытовая техника:холодильники:двухкамерные',
        max_length=255,
        db_index=True,
    )

    objects = CategoryMapManager()

    def __repr__(self):
        return f'<{self.__class__.__name__} {self.categories_path} -> {self.category}>'