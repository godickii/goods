from goods.installment import DEFAULT_INSTALLMENT_PLAN


class Categorizer:

    CATEGORY_MAP = {
        'www.technopark.ru.xml':
            {
                '_meta':
                    {
                        'name': 'www.technopark.ru',
                        'feed_url': 'https://www.technopark.ru/price/halva-pop.xml',
                        'installment_plan': DEFAULT_INSTALLMENT_PLAN,
                        'description': 'Купи 2 по цене 4х и получи третий в подарок',
                        'priority': 10,
                        'label_img': 'technopark.png'
                    },
                'крупная бытовая:холодильники': 'бытовая техника:холодильники',
            },
        'feed.mgcom.ru-knowhow.xml':
            {
                '_meta':
                    {
                        'name': 'ноу-хау.рф',
                        'feed_url': 'http://feed.mgcom.ru/yandexmarket/knowhow/halva/f19c2',
                        'installment_plan': DEFAULT_INSTALLMENT_PLAN,
                        'description': 'Ложь, пиздёж и промо-акция',
                        'priority': 10,
                        'label_img': 'know_how.png'
                    },
                'каталог товаров:смартфоны': 'электроника:мобильные телефоны',
            },
        'feed.mgcom.ru-hoff.xml':
            {
                '_meta':
                    {
                        'name': 'Hoff',
                        'feed_url': 'http://feed.mgcom.ru/yandexmarket/hoff/moscow/dda93',
                        'installment_plan': DEFAULT_INSTALLMENT_PLAN,
                        'description': 'мебель',
                        'priority': 11,
                        'label_img': 'hoff.png'
                    },
                'гостиная': 'мебель:гостиная',
                'спальня': 'мебель:спальня',
                'кухня': 'мебель:кухня',
            },
        'svyaznoy.xml':
            {
                '_meta':
                    {
                        'name': 'связной',
                        'feed_url': 'http://feed.mgcom.ru/yandexmarket/svyaznoy/promofocus/04ee9',
                        'installment_plan': DEFAULT_INSTALLMENT_PLAN,
                        'description': '9.5 минут рассрочки, фильм "9 с половиной минут" в подарок',
                        'priority': 10,
                        'label_img': 'svyaznoi.png'
                    },
                'техника для дома:утюги': 'бытовая техника:утюги',
                'техника для дома:пылесосы': 'бытовая техника:пылесосы',
                'техника для дома:водонагреватели': 'бытовая техника:водонагреватели',
                'фотоаппараты': 'электроника:фотоаппараты',
                'мобильные телефоны': 'электроника:мобильные телефоны'
            },
        'xml.holodilnik.ru-all.xml':
            {
                '_meta':
                    {
                        'name': 'holodilnik.ru',
                        'feed_url': 'https://xml.holodilnik.ru/xml/?from=ozon',
                        'installment_plan': DEFAULT_INSTALLMENT_PLAN,
                        'description': 'хана кошелёчку, расстрел за просрочку, ну или заточку, прямо в глазочку, веночку на кочку, и к ангелочку',
                        'priority': 10,
                        'label_img': 'holodilnik.png'
                    },
                'бытовая техника для дома:техника для ухода за одеждой:утюги': 'бытовая техника:утюги',
                'бытовая техника для дома:техника для уборки:пылесосы': 'бытовая техника:пылесосы',
                'бытовая техника для дома:техника для уборки:пылесосы беспроводные': 'бытовая техника:пылесосы',
                'бытовая техника для дома:техника для уборки:пылесосы моющие': 'бытовая техника:пылесосы',
                'бытовая техника для дома:техника для уборки:роботы-пылесосы': 'бытовая техника:пылесосы',
                'бытовая техника для дома:климатическая техника:водонагреватели': 'бытовая техника:водонагреватели',
                'цифровая техника, товары для офиса:фото и видеосъемка:цифровые фотоаппараты': 'электроника:фотоаппараты',
                'цифровая техника, товары для офиса:смартфоны, планшеты и гаджеты:мобильные телефоны': 'электроника:телефоны',
                'холодильники, морозильники:однокамерные холодильники': 'бытовая техника:холодильники',
                'холодильники, морозильники:двухкамерные холодильники': 'бытовая техника:холодильники',
                'холодильники, морозильники:холодильники side by side': 'бытовая техника:холодильники',
                'холодильники, морозильники:многокамерные холодильники': 'бытовая техника:холодильники',
                'холодильники, морозильники:минихолодильники': 'бытовая техника:холодильники',
                'посудомоечные, стиральные машины:стиральные машины': 'бытовая техника:стиральные машины',
                'посудомоечные, стиральные машины:стиральные машины с сушкой': 'бытовая техника:стиральные машины',
                'телевизоры, dvd, аудио-видео:телевизоры': 'электроника:телевизоры',
                'бытовая техника для дома:климатическая техника:кондиционеры': 'бытовая техника:кондиционеры',
            },
        'santehnika-online.ru.xml':
            {
                '_meta':
                    {
                        'name': 'сантехника-онлайн',
                        'feed_url': 'https://santehnika-online.ru/xml_feed/yandex/yml_export.xml',
                        'installment_plan': DEFAULT_INSTALLMENT_PLAN,
                        'description': '',
                        'priority': 10,
                        'label_img': 'santehnika.png'
                    },
                'климат:кондиционеры': 'бытовая техника:кондиционеры',
                'мебель для ванной': 'мебель:ванная',
            },
        # 'lamoda.xml':
        #     {
        #         '_meta':
        #             {
        #                 'name': 'lamoda',
        #                 'installment_plan': DEFAULT_INSTALLMENT_PLAN,
        #                 'description': '',
        #                 'priority': 1,
        #                 'label_img': 'santehnika.png'
        #             },
        #     },
    }

    def items(self):
        return self.CATEGORY_MAP.items()