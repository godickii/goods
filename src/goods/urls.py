from django.urls import path

from . import views

app_name = 'demo'

urlpatterns = [
    path('', views.DemoIndexView.as_view(), name='demo-index'),
    path('<slug:category_slug>/', views.DemoIndexView.as_view(), name='browse-category')
]
