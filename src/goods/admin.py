from django.contrib import admin
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponseRedirect
from django.urls import path

from goods.filters import CategoryFilter
from goods.models import Shop, Category, Offer, CategoryMap


class CategoryMapTabularInline(admin.TabularInline):
    model = CategoryMap
    fields = (
        'categories_path',
        'category',
    )


@admin.register(Shop)
class ShopAdmin(admin.ModelAdmin):
    change_form_template = 'goods/shop_changeform.html'
    cha = 'goods/change_list.html'
    inlines = [
        CategoryMapTabularInline
    ]
    fields = (
        'name',
        'picture',
        'feed_url',
        'priority',
        'installment_plan',
        'is_promotional',
    )

    list_filter = (
        'is_promotional',
    )


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_filter = (
        'is_promotional',
    )


@admin.register(Offer)
class OfferAdmin(admin.ModelAdmin):
    list_display_links = list_display = (
        'is_visible',
        'partner',
        'category',
        'name',
        'vendor',
        'model',
        'price',
        'months',
        'price_per_month',
        'categories_path',
    )

    list_filter = (
        'is_visible',
        'is_promotional',
        CategoryFilter,
        'partner',
    )

    def export(self, request):
        CategoryMap.objects.reindex()
        self.message_user(request, 'Пересчет категории по товарам выполнен!')
        return HttpResponseRedirect(request.META["HTTP_REFERER"])

    def get_urls(self):
        urls = super(OfferAdmin, self).get_urls()
        my_urls = [
            path("reindex/", staff_member_required(self.export)),
        ]
        return my_urls + urls
