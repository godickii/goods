import re, math
from django import template

register = template.Library()


@register.filter
def price_from(value, months):
    months = int(re.findall('\d+', months)[0])
    return math.ceil(int(value)/months)


@register.simple_tag
def query_transform(request, **kwargs):
    updated = request.GET.copy()
    for k in kwargs.keys():
        if k in updated:
            del updated[k]
    updated.update(kwargs)
    return updated.urlencode()
