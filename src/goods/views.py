from django.views.generic import TemplateView

from goods.fakedb import Shop, FILES, SHOP_COLLECTION_FACTORY
from django import forms

#shop = Shop('xml.holodilnik.ru.xml')

# @listify
# def get_all_offers_info():
#     for file in FILES:
#         shop = Shop(file)
#         for path, offer in shop.get_offers_annotated_by_categories_path():
#             yield (shop.name, ' / '.join(path), offer)

# all_offers_info = get_all_offers_info()


class SearchForm(forms.Form):
    query = forms.CharField(
        label=False,
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Найти товар',
                'class': 'search-goods-input'  # form-control'
            }),
        required=False)
    price_min = forms.IntegerField(label='Цена от', required=False, widget=forms.HiddenInput())
    price_max = forms.IntegerField(label='до', required=False, widget=forms.HiddenInput())
    months_min = forms.IntegerField(label='Месяцев от', required=False, widget=forms.HiddenInput())
    months_max = forms.IntegerField(label='Месяцев до', required=False, widget=forms.HiddenInput())
    price_per_month_from = forms.IntegerField(label='Платёж в месяц от', required=False, widget=forms.HiddenInput())
    price_per_month_to = forms.IntegerField(label='Платёж в месяц до', required=False, widget=forms.HiddenInput())


class IndexView(TemplateView):
    template_name = "index.html"


class DemoIndexView(TemplateView):
    template_name = "demo.html"

    def get_context_data(self, **context):
        shop = SHOP_COLLECTION_FACTORY.get_shop_collection()

        search_form = SearchForm(self.request.GET)
        # check whether it's valid:
        context['search_form'] = search_form

        context['top_categories'] = shop.get_sub_categories()
        #current_category_id = self.request.GET.get('category_id')
        current_category_id = context.get('category_slug')
        if current_category_id:
            context['subcategories'] = shop.get_sub_categories(current_category_id)
            context['category_title'] = shop.category_by_id[current_category_id]['text']
            offers = shop.get_offers(current_category_id)
        else:
            context['category_title'] = 'Все товары'
            offers = shop.get_all_offers()

        filters = {}
        if search_form.is_valid():
            filters.update(search_form.cleaned_data)

        if 'order_by' in self.request.GET:
            filters['order_by'] = self.request.GET['order_by']

        filter_data = {v: [int(offer[v]) for offer in offers] for v in ['price', 'months', 'price_per_month']}
        prices = filter_data['price']
        context['price_min'] = min(prices)
        context['price_max'] = max(prices)

        months = filter_data['months']
        context['months_min'] = min(months)
        context['months_max'] = max(months)

        prices_per_month = filter_data['price_per_month']
        context['price_per_month_from'] = min(prices_per_month)
        context['price_per_month_to'] = max(prices_per_month)

        offers = shop.apply_filters(filters, offers)

        offers = list(offers)[:200]
        context['offers'] = offers

        #context['all_offers_info'] = all_offers_info

        return context

