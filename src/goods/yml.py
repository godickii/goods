import os
import xmltodict
import gzip

from project.settings import BASE_DIR
from goods.util import listify


FILES_DIR = os.path.join(BASE_DIR, 'files')


class FileParser:
    def __init__(self, offer_callback):
        self._current_shop_category_by_id = {}
        self._current_shop_offers_by_category_id = {}
        self._offer_callback = offer_callback
        self._total_offers_count = 0
        self._total_offers_matched = 0

    def parse_file(self, filename):
        print(f'Parsing {filename} ...')

        if filename.endswith('gz'):
            file = gzip.open(os.path.join(FILES_DIR, filename), 'rb')
        else:
            file = open(os.path.join(FILES_DIR, filename), 'rb')

        d = xmltodict.parse(
            file,
            item_depth=4,
            item_callback=self._item_callback
        )

        #print(f'{self._total_offers_count} offers processed.')

    def get_sub_categories(self, parent_id=None):
        """
        Возвращает подкатегории.
        Если parent_id = None, возвращает категории верхнего уровня.
        """
        result = []
        for c in self._current_shop_category_by_id.values():
            if c.get('parentId') == parent_id:
                result.append(c)
        return result

    # def get_printable_tree(self, parent_id=None, indent=0, annotate_with_goods_count=True):
    #     for category in self.get_sub_categories(parent_id=parent_id):
    #         yield '%s%s' % ('\t' * indent, category['text']) + (annotate_with_goods_count and ' (%d)' % self._current_shop_offers_by_category_id.get(category['id'], 0) or '')
    #         yield from self.get_printable_tree(category['id'], indent + 1)

    def get_printable_tree(self, parent_id=None, prefix='', annotate_with_goods_count=True):
        for category in self.get_sub_categories(parent_id=parent_id):
            # только не пустые категории
            if self._current_shop_offers_by_category_id.get(category['id'], 0):
                yield '%s%s' % (prefix, category['text']) + (annotate_with_goods_count and ' (%d)' % self._current_shop_offers_by_category_id.get(category['id'], 0) or '')
            yield from self.get_printable_tree(category['id'], prefix + category['text'] + ':')

    def _item_callback(self, a, b):
        current_path = (a[2][0], a[3][0])  # FIXME: magic numbers
        # print(current_path)
        # print('a =', a)
        # print('b =', b)
        # print()
        if current_path == ('categories', 'category'):
            category = a[3][1]  # FIXME: magic number
            category['text'] = b.strip().lower()
            self._current_shop_category_by_id[category['id']] = category
            # проверка что categoryId хэшируется
            # print(category.get('parentId'), hash(category.get('parentId')))

        elif current_path == ('offers', 'offer'):
            offer = a[3][1]  # FIXME: magic number
            offer.update(b)
            offer['price'] = int(float(offer['price']))
            if isinstance(offer["categoryId"], list):
                offer['categoryId'] = offer['categoryId'][0]

            self._total_offers_count += 1
            if not self._total_offers_count % 100:
                print(f'.', end='', flush=1)
                # print(f' ... {self._total_offers_count} offers loaded')
            if isinstance(offer.get('picture'), list):
                offer['picture'] = offer['picture'][0]
            category_path = list(reversed(self._current_shop_get_path_for_offer_reversed(offer)))
            offer['category_path'] = category_path\

            category_id = offer['categoryId']
            self._current_shop_offers_by_category_id.setdefault(category_id, 0)
            self._current_shop_offers_by_category_id[category_id] += 1

            self._offer_callback(offer)

        return True

    @listify
    def _current_shop_get_path_for_offer_reversed(self, offer):
        category_id = offer['categoryId']
        # print(f'Offer.{category_id=} {offer["id"]=}')

        while category_id:
            try:
                category = self._current_shop_category_by_id[category_id]
            except TypeError:
                print(f'{category_id=}')
                raise
            except KeyError:
                # traceback.format_exc()
                # TODO
                yield '(null)'
                category_id = None
            else:
                yield category['text']
                category_id = category.get('parentId')
