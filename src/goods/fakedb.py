#!/usr/bin/env python

import sys
import os
import xmltodict
import pickle
import hashlib

from goods.categorizing import Categorizer

try:
    from src.project.settings import BASE_DIR
except ImportError:
    from project.settings import BASE_DIR

try:
    from src.goods.util import listify
except ImportError:
    from goods.util import listify

from django.conf import settings

FILES_DIR = os.path.join(BASE_DIR, 'files')
SHOP_COLLECTION_DUMP_FILENAME = os.path.join(FILES_DIR, 'shop_collection.pickle')

LIMIT_OFFERS_IN_CATEGORY_BY_SHOP = 20000


def translit(text):
    # https://stackoverflow.com/questions/14173421/use-string-translate-in-python-to-transliterate-cyrillic
    symbols = (u"абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ",
               u"abvgdeejzijklmnoprstufhzcss_y_euaABVGDEEJZIJKLMNOPRSTUFHZCSS_Y_EUA")
    tr = {ord(a):ord(b) for a, b in zip(*symbols)}
    return text.translate(tr)


def md5(s):
    return hashlib.md5(s.encode('utf-8')).hexdigest()


def generate_our_category_id(category_name):
    return translit(category_name).replace(' ', '_')


class Directory:
    def get_sub_categories(self, parent_id=None):
        """
        Возвращает подкатегории.
        Если parent_id = None, возвращает категории верхнего уровня.
        """
        result = []
        for c in self.category_by_id.values():
            #print(c.get('@parentId'), parent_id, c.get('@parentId') == parent_id)
            if c.get('@parentId') == parent_id:
                result.append(c)
        return result

    def get_offers(self, category_id):
        return self.offers_by_category_id.get(category_id, [])

    def get_all_offers(self):
        return self.offer_by_id.values()

    @listify
    def get_path_for_offer_reversed(self, offer):
        category_id = offer['categoryId']
        while category_id:
            category = self.category_by_id[category_id]
            yield category['text']
            category_id = category.get('@parentId')

    @listify
    def get_offers_annotated_by_categories_path(self):
        offers = self.get_all_offers()
        for o in offers:
            yield list(reversed(self.get_path_for_offer_reversed(o))), o

    def get_printable_tree(self, parent_id=None, indent=0, annotate_with_goods_count=True):
        for category in self.get_sub_categories(parent_id=parent_id):
            yield '%s%s' % ('\t' * indent, category['text']) + (annotate_with_goods_count and ' (%d)' % len(self.offers_by_category_id.get(category['id'], [])) or '')
            yield from self.get_printable_tree(category['id'], indent + 1)

    @staticmethod
    def apply_filters(filters, offers):
        #query = filters.get('query')
        #if query in filters
        months_min = filters.get('months_min', 0)
        if months_min:
            offers = filter(lambda offer: 'months' in offer and offer['months'] >= int(months_min), offers)

        months_max = filters.get('months_max', 0)
        if months_max:
            offers = filter(lambda offer: 'months' in offer and offer['months'] <= int(months_max), offers)

        price_min = filters.get('price_min', 0)
        if price_min:
            offers = filter(lambda offer: int(offer['price']) >= int(price_min), offers)

        price_max = filters.get('price_max', 0)
        if price_max:
            offers = filter(lambda offer: int(offer['price']) <= int(price_max), offers)

        price_per_month_from = filters.get('price_per_month_from', 0)
        if price_per_month_from:
            offers = filter(
                lambda offer: 'price_per_month' in offer and int(offer['price_per_month']) >= int(price_per_month_from),
                offers
            )

        price_per_month_to = filters.get('price_per_month_to', 0)
        if price_per_month_to:
            offers = filter(
                lambda offer: 'price_per_month' in offer and int(offer['price_per_month']) <= int(price_per_month_to),
                offers
            )

        order_by = filters.get('order_by', '')
        if order_by == 'price':
            offers = sorted(
                offers, key=lambda offer: int(offer['price'])
            )
        elif order_by == '-price':
            offers = sorted(
                offers, key=lambda offer: -int(offer['price'])
            )
        elif order_by == 'price_per_month':
            offers = sorted(
                offers, key=lambda offer: int(offer.get('price_per_month', 999999999))
            )
        elif order_by == '-price_per_month':
            offers = sorted(
                offers, key=lambda offer: -int(offer.get('price_per_month', 0))
            )
        else:
            offers = sorted(
                offers, key=lambda offer: (-offer['shop']['meta']['priority'], -offer.get('price_per_month', 0))
            )

        query = filters['query']

        def match_query(offer):
            words = query.lower().split()
            for word in words:
                if word in offer.get('name', '').lower():
                    return True

            for word in words:
                if word in offer.get('vendor', '').lower():
                    return True

            for word in words:
                if word in offer.get('model', '').lower():
                    return True

        if query:
            offers = filter(match_query, offers)

        return offers


class Shop(Directory):
    """
    Парсит YML в более привычные нам структуры данных

    Пример category:
    OrderedDict([('@id', '213'), ('@parentId', '210'), ('#text', 'Очищение')]),

    Пример offer:
    OrderedDict([('@id', '107'),
                 ('@available', 'true'),
                 ('url',
                  'https://merries.info/podguzniki-i-trusiki/podguzniki-merries-m-22-sht-dlya-malyshey-ot-6-do-11-kg.html'),
                 ('price', '590'),
                 ('currencyId', 'RUB'),
                 ('categoryId', '80'),
                 ('picture',
                  ['https://merries.info/image/tm_00001926_podguzniki_m_6_11_kg_22_sht.png',
                   'https://merries.info/image/tm_00001926_imidzh_dlya_podguznikov_nb_s_m_l_xl.png',
                   'https://merries.info/image/tm_00001926_podguzniki_nb_s_m_l_xl.jpg']),
                 ('delivery', 'true'),
                 ('cpa', '1'),
                 ('name', 'Merries подгузники мал. M (6-11 кг), 22 шт'),
                 ('vendor', 'Merries'),
                 ('model', '4901301509079')])
    """
    def __init__(self, filename, delete_empty_categories=True):
        d = xmltodict.parse(open(os.path.join(FILES_DIR, filename), encoding='utf8').read())

        self.name = d['yml_catalog']['shop']['name']

        self.category_by_id = {}
        self.offer_by_id = {}
        self.offers_by_category_id = {}

        for category in d['yml_catalog']['shop']['categories']['category']:
            category['id'] = category['@id']
            del category['@id']
            category['text'] = category['#text'].lower()
            del category['#text']
            self.category_by_id[category['id']] = category

        for offer in d['yml_catalog']['shop']['offers']['offer']:
            self.offer_by_id[offer['@id']] = offer
            self.offers_by_category_id.setdefault(offer['categoryId'], [])
            self.offers_by_category_id[offer['categoryId']].append(offer)

        not_empty_category_ids = set(self.offers_by_category_id.keys())
        changed = True
        while changed:
            changed = False
            for category_id in set(not_empty_category_ids):
                parent_id = self.category_by_id.get(category_id, {}).get('@parentId')
                if parent_id not in not_empty_category_ids:
                    not_empty_category_ids.add(parent_id)
                    changed = True
                    continue

        #print(self.offers_by_id.keys())
        # Удаляем пустые категории
        if delete_empty_categories:
            for c_id in list(self.category_by_id.keys()):
                if c_id not in not_empty_category_ids:
                    del self.category_by_id[c_id]


FILES = \
"""
eyekraft.ru.xml
feed.mgcom.ru.xml
www.585zolotoy.ru.xml
www.akusherstvo.ru.xml
www.rendez-vous.ru.xml
www.technopark.ru.xml
www.timecode.ru.xml
zarina.ru.xml
svyaznoy.xml
xml.holodilnik.ru-all.xml
santehnika-online.ru.xml
tvoydom.ru.xml
""".split()

#lamoda.xml - не хватило с ulimit -Sv 16000000
#xml.holodilnik.ru.xml - содержиться в xml.holodilnik.ru-all.xml

#FILES = ['www.technopark.ru.xml']
# lamoda.xml


class ShopCollection(Directory):
    def __init__(self):

        self.category_by_id = {}
        self.offer_by_id = {}
        self.offers_by_category_id = {}

        for filename, map_rules in Categorizer().items():
            #print(f'Loading {filename} ...')

            shop = {
                'name': 'noname0000', #, d['yml_catalog']['shop']['name'],
                'meta': map_rules['_meta']
            }

            self.installment_plan = shop['meta']['installment_plan']

            current_shop_category_by_id = {}
            current_shop_offers_by_category_id = {}

            @listify
            def current_shop_get_path_for_offer_reversed(offer):
                category_id = offer['categoryId']
                while category_id:
                    try:
                        category = current_shop_category_by_id[category_id]
                    except KeyError:
                        #traceback.format_exc()
                        #TODO
                        yield '(null)'
                        category_id = None
                    else:
                        yield category['text']
                        category_id = category.get('parentId')


            self.total_offers_count = 0
            self.total_offers_matched = 0

            def item_callback(a, b):
                current_path = (a[2][0], a[3][0])
                # print(current_path)
                # print('a =', a)
                # print('b =', b)
                # print()
                if current_path == ('categories', 'category'):
                    category = a[3][1]
                    category['text'] = b.strip().lower()
                    current_shop_category_by_id[category['id']] = category
                elif current_path == ('offers', 'offer'):
                    offer = a[3][1]
                    offer.update(b)
                    offer['price'] = int(float(offer['price']))

                    # количество месяцев, для которых доступна рассрочка
                    price = offer['price']
                    months = None

                    for cur_amount, cur_month in sorted(self.installment_plan.items(), key=lambda pair:pair[0]):
                        if price >= cur_amount:
                            months = cur_month

                    if months:
                        offer['months'] = months
                        import math
                        offer['price_per_month'] = int(math.ceil(price / months))


                    self.total_offers_count += 1
                    if not self.total_offers_count % 1000:
                        print(f' ... {self.total_offers_count} offers loaded')
                    if isinstance(offer.get('picture'), list):
                        offer['picture'] = offer['picture'][0]
                    offer['shop'] = shop
                    category_path = list(reversed(current_shop_get_path_for_offer_reversed(offer)))
                    matched_our_category = None
                    for i in range(len(category_path)):
                        category_path_str = ':'.join(category_path[:i + 1])
                        checked_match = map_rules.get(category_path_str)
                        if checked_match:
                            #print(category_path_str, ' --> ', checked_match)
                            matched_our_category = checked_match

                    if matched_our_category:
                        self.total_offers_matched += 1
                        our_category_id = generate_our_category_id(matched_our_category)
                        self.category_by_id[our_category_id] = {
                            'text': matched_our_category,
                            'id': our_category_id
                        }
                        self.offer_by_id[offer['id']] = offer
                        self.offers_by_category_id.setdefault(our_category_id, [])
                        current_shop_offers_by_category_id.setdefault(our_category_id, [])  # для лимита
                        if len(current_shop_offers_by_category_id[our_category_id]) < LIMIT_OFFERS_IN_CATEGORY_BY_SHOP:
                            self.offers_by_category_id[our_category_id].append(offer)
                            current_shop_offers_by_category_id[our_category_id].append(offer)  # для лимита


                return True

            print(f'Parsing {filename} ...')

            d = xmltodict.parse(
                open(os.path.join(FILES_DIR, filename), 'rb'),
                item_depth=4,
                item_callback=item_callback
            )

            print(f'Matched {self.total_offers_matched} of {self.total_offers_count}.')
            # for category in d['yml_catalog']['shop']['categories']['category']:
            #     category['id'] = category['@id']
            #     del category['@id']
            #     category['text'] = category['#text'].lower()
            #     del category['#text']
            #     current_shop_category_by_id[category['id']] = category



if __name__ == '__main__' or settings.BUILD_SHOP_COLLECTION_EVERY_TIME:
    # TODO: mb move this to a django management command?
    # сборка дампа
    SHOP_COLLECTION = ShopCollection()
    f = open(SHOP_COLLECTION_DUMP_FILENAME, 'wb')
    pickle.dump(SHOP_COLLECTION, f)
    f.close()


class CustomUnpickler(pickle.Unpickler):

    def find_class(self, module, name):
        if name == 'ShopCollection':
            return ShopCollection
        return super().find_class(module, name)


class ShopCollectionFactory:
    def __init__(self):
        self.shop_collection = None

    def get_shop_collection(self):
        if self.shop_collection is None:
            self.shop_collection = CustomUnpickler(open(SHOP_COLLECTION_DUMP_FILENAME, 'rb')).load()

        return self.shop_collection


SHOP_COLLECTION_FACTORY = ShopCollectionFactory()

# https://stackoverflow.com/questions/27732354/unable-to-load-files-using-pickle-and-multiple-modules
# SHOP_COLLECTION = CustomUnpickler(open(SHOP_COLLECTION_DUMP_FILENAME, 'rb')).load()


if __name__ == '__main__':
    try:
        command = sys.argv[1]
    except IndexError:
        command = None

    if command == 'index':
        for file in FILES:
            print(f'indexing {file}...')
            shop = Shop(file, delete_empty_categories=True)
            with open(os.path.join(FILES_DIR, 'categories', '%s.txt' % file), 'wt') as f:
                f.write(('=== %s, файл %s ===\n' % (shop.name, file)))
                f.write('\n'.join(shop.get_printable_tree()))
            #for path, offer in shop.get_offers_annotated_by_categories_path():
            #    print(path, offer['name'], offer['price'], offer['url'])
            #for c in shop.get_offers():
            #    print(c['id'], c['text'])
