#!/bin/bash
cd "$(dirname "$0")"
cd ..

HOST=goods-prototype.lesha.info
USER=al

# Сборка дампа
# echo "Собираю дамп."
# set -x
# cd src
# PYTHONPATH=. ../venv/bin/python goods/fakedb.py build
# cd ..




#src/manage.py collectstatic --noinput
rsync -avzr deploy src static templates files requirements.txt requirements-dev.txt ${USER}@${HOST}:goods --exclude='__pycache__' --exclude='settings_base.py' --exclude='settings_local.py'
ssh ${USER}@${HOST} goods/venv/bin/python goods/src/manage.py migrate
ssh ${USER}@${HOST} sudo supervisorctl restart goods
